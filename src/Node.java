
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node firstChild, Node nextSibling) {
      this.name = n;
      this.firstChild = firstChild;
      this.nextSibling = nextSibling;
   }

   public static String checkMainErrors(String s) {
      boolean space = false;
      boolean name = false;
      int brackets = 0;
      for (int i = 0; i < s.length(); i++) {
         if (s.charAt(i) == '(') {
            brackets++;
         }
         if (s.charAt(i) == ')') {
            brackets--;
            space = false;
            if (brackets < 0) {
               throw new RuntimeException("Weird brackets in " + s);
            }
         }if (s.charAt(i) == '(' || s.charAt(i) == ',') {
            space = false;
         }if(s.charAt(i) == ')' && s.charAt(i+1) == ')') {
            throw new RuntimeException("Double brackets in " + s);
         }if(s.charAt(i) == '(' && s.charAt(i+1) == ')') {
            throw new RuntimeException("Can not have an empty child in " + s);
         }if(s.charAt(i) == ',' && s.charAt(i+1) == ')') {
            throw new RuntimeException("No sibling after a comma in " + s);
         }if(s.charAt(i) == ',' && s.charAt(i+1) == ',') {
            throw new RuntimeException("Can not have two commas without a sibling in " + s);
         }if(s.charAt(i) == '(' && s.charAt(i+1) == ',') {
            throw new RuntimeException("Can not have a comma without left sibling in " + s);
         }if(s.charAt(i) != ',' && s.charAt(i) != '(' && s.charAt(i) != ')' && s.charAt(i) != ' ') {
            name = true;
         }
         if(s.charAt(i) == ' ') {
            space = true;
         }
         if (name && space && s.charAt(i) != ',' && s.charAt(i) != '(' && s.charAt(i) != ')' && s.charAt(i) != ' ') {
            throw new RuntimeException("Can not have a space between nodes in " + s);
         }
      }
      s = s.replaceAll("[ \t]", "");
      if (s.equals("") ) {
         throw new RuntimeException("Can not build a tree from empty string");
      }
      return s;
   }

   //inspiratsioon allikast https://pastebin.com/trMY6MxL
   public static Node parsePostfix (String s) {
      s = checkMainErrors(s);
      Stack<Node> stack = new Stack<>();
      int nodeNames = 0;
      Node usableNode = new Node(null, null, null);
      StringTokenizer st = new StringTokenizer(s.trim(), "(),", true);
      while(st.hasMoreTokens()){
         String token = st.nextToken();
         if (token.equals("(")) {
            stack.push(usableNode);
            usableNode.firstChild = new Node(null, null, null);
            usableNode = usableNode.firstChild;
         } else if (token.equals(")")) {
            usableNode = stack.pop();
         } else if (token.equals(",")) {
            if (stack.empty()) {
               throw new RuntimeException("Parent node can not have a sibling in " + s);
            }
            usableNode.nextSibling = new Node(null, null, null);
            usableNode = usableNode.nextSibling;
         } else {
            nodeNames++;
            usableNode.name = token;
         }
      }
      if (nodeNames == 0) {
         throw new RuntimeException("No nodes in " + s);
      }
      return usableNode;
   }

   //allikas https://pastebin.com/trMY6MxL
   public String leftParentheticRepresentation() {
      StringBuilder left = new StringBuilder();
      left.append(this.name);
      if(this.firstChild != null){
         left.append("(");
         left.append(this.firstChild.leftParentheticRepresentation());
         left.append(")");
      }
      if(this.nextSibling != null){
         left.append(",");
         left.append(this.nextSibling.leftParentheticRepresentation());
      }
      return left.toString();
   }


   public String xmlPresentation(int nodeNumber) {
      StringBuilder xml = new StringBuilder();
      for (int i = 1; i < nodeNumber; i++) {
         xml.append("\t");
      }
      xml.append("<L").append(nodeNumber).append("> ").append(this.name);
      if(this.firstChild != null){
         xml.append("\n").append(this.firstChild.xmlPresentation(nodeNumber + 1)).append("\n");
         for (int i = 1; i < nodeNumber; i++) {
            xml.append("\t");
         }
         xml.append("</L").append(nodeNumber).append(">");
      }
      if (this.firstChild == null) {
         xml.append(" </L").append(nodeNumber).append("> ");
      }
      if(this.nextSibling != null){
         xml.append("\n").append(this.nextSibling.xmlPresentation(nodeNumber));
      }
      return xml.toString();
   }

   public static void main (String[] param) {
      String s = "(((512,1)-,4)*,(-6,3)/)+";
      Node t = Node.parsePostfix (s);
      String v = t.xmlPresentation(1);
      String left = t.leftParentheticRepresentation();
      System.out.println (left + " ==> ");
      System.out.println(v);
      //System.out.println(t.counter);
   }
}

